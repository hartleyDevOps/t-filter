# t-filter Specification

### Component Use

``` html

<t-filter
	resources="{{resources}}"
	options="{{filters}}"
>

</t-filter>

```


### Options to Component

```javascript

	var resources = {
		"isOpenForMobile": false,
	    "icons": {
	      "filter": "filter-icon"
	    },
	    "label": {
	      "heading": "Filter Activities By",
	      "reset": "Reset Filter"
	    },
	    "events": {
	    	"resetFilter": "_resetFilters"
	    }
	  }

```

```javascript

	var filters = [
		  {
		    "label": "Hotel/Activity Name",
		    "type": "Autosuggest",
		    "placeholder": "Search by activity/hotel name",
		    "isOpen": true,
		    "showMoreAfter": 5,
		    "onChange": "_updateFilterQuery",
		    "selectedValue": null,
		    "list": [
		      {
		        "label": "Las Vegas Hilton Delux"
		      },
		      {
		        "label": "Las Vegas Hilton Delux"
		      },
		      {
		        "label": "Las Vegas Hilton Delux"
		      }
		    ]
		  },
		  {
		    "label": "Price/Distance",
		    "type": "Range",
		    "isOpen": false,
		    "showMoreAfter": 5,
		    "onChange": "_updateFilterQuery",
		    "selectedValue": "35.10",
		    "minPrice": {
		      "currency": "USD",
		      "value": "9.10"
		    },
		    "maxPrice": {
		      "currency": "USD",
		      "value": "158.81"
		    }
		  },
		  {
		    "label": "Rating",
		    "type": "Options",
		    "isOpen": false,
		    "showMoreAfter": 5,
		    "onChange": "_updateFilterQuery",
		    "options": [
		      {
		        "rating": 5,
		        "selected": false,
		        "count": 12
		      },
		      {
		        "rating": 4,
		        "selected": false,
		        "count": 22
		      },
		      {
		        "rating": 3,
		        "selected": false,
		        "count": 34
		      },
		      {
		        "rating": 2,
		        "selected": false,
		        "count": 8
		      },
		      {
		        "rating": 1,
		        "selected": false,
		        "count": 5
		      }
		    ]
		  },
		  {
		    "label": "Category/Brand/Amenities",
		    "type": "Options",
		    "isOpen": false,
		    "showMoreAfter": 5,
		    "onChange": "_updateFilterQuery",
		    "autosuggest": {
		      "enabled": true,
		      "placeholder": "Search by category name"
		    },
		    "options": [
		      {
		        "label": "Family Friendly",
		        "selected": false
		      },
		      {
		        "label": "Transfers &amp; Ground Transport",
		        "selected": false
		      }
		    ]
		  }
		]
		
```


## Important Information

- Misc items to be handled from CSS like:
1. List card background color
2. Mixin (to control text color, font etc)
3. Button color etc.


## Test Cases

- Basic validation for filter

## Steps to Start
- Set Github repository at your end for this project, we will merge them later
- APIs Integration - Use Tavisca's APIs for integration purpose.

## Performance standard
- Any component if opened via [web page tester](https://www.webpagetest.org/), it should load under 500ms (milli seconds).

## Documents
- Visual designs for search components - https://projects.invisionapp.com/share/6E9PJ7R4Q#/screens/212067485
- API access : Url - http://demo.travelnxt.com/dev
- Tavisca Elements - https://github.com/atomelements and https://github.com/travelnxtelements
- Vaadin elements - https://vaadin.com/docs/-/part/elements/elements-getting-started.html
- Google - https://elements.polymer-project.org/browse?package=google-web-components
- Tavisca Web component style Guide - https://drive.google.com/open?id=0B7BT_2nBFNYVR2tscE9pRnVJYmc

## Ballpark Estimates

20 Days / 4 Weeks
- Estimates are including above requrement with responsive design.
- Estimates are for single resourse.
- These are ballpark estimates and can be more/less while actual development.
